# What is Docker

    -Docker is a tool for running applications in an isolated environment
    -Similar to Virtual machine
    -App run in same environment
    -Just works

## Benefits

    - Run container in seconds instead of minutes.
    - Less ressources results less disk spaces.
    - Uses less Memory.
    - Does not need full OS.
    - Deployment.
    - Testing.

# Containers vs Virtual Machine

## Containers

are an abstraction at the app layer that packages code and dependencies together. Multiple containers can run on the same machine and share the OS kernel with other containers, each running as isolated processes in user space.

## Virtual machines

are an abstraction of physical hardware turning one server into many servers. The hypervisor allows multiple VMs to run on a single machine. Each VM includes a full copy of an operating system, the application, necessary binaries and libraries - taking up tens of GBs. VMs can also be slow to boot.
