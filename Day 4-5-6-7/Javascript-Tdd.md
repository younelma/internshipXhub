# Javascript TDD using Jest

**Overview :** Test Driven Development (TDD) = Test First Development (TFD) + Refactoring basic concepts.

## Basic Concepts :

TDD is a programming technique.
Simply put the steps into TDD are as follows:
1- Add a quick test. _basically just enough code to fail_
2- Run the test. _It should fail_
3- Write functional code.
4- Finally, run the test to check whether it has passed.
5- Re-factor until tests pass. _to continue with further development_

The idea behind TDD is instead of writing functional code first and then the testing code if at all, you first write the test code before the functional code.
