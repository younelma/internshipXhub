# Javascript for React :

    1- Short Conditionals: &&, ||, TenaryOperator.
    2- Array Methods: .map(), .filter(), .reducer() ...
    3- Object Tricks: Property Shorthand, Destructring, Spread Operator.
    4- Promises + Async/Await Syntax.
    5- ES Modules + Import/Export Syntax.

## Basic Concepts

Learn how to work with arrays, objects, functions, loops, is/else statements and more.

## ES6

ES6, released in 2015, added many powerful new features to the language. You'll learn these new features, including arrow functions, destructuring, classes, promises, and modules.

## Basic Algorithm Scripting

I learn from the FreeCodeCamp course the fundamentals of algorithmic by writing algorithms that do everything from converting temperatures to handling complex 2D arrays.

## OOP (Object Oriented Programming)

OOP is one of the major approaches to the software development process. In OOP, objects and classes organize code to describe things and what they can do.

I've learned the basic principles of OOP in Javascript, including the _this_ keyword, prototype chains, constructors, and inheritance.

## Functional Programming

in Functional Programming, code is organized into smaller, basic functions that can be combined to build complex programs.

core concepts of Functional Programming including pure functions, how to avoid mutations, and how to write cleaner code with methods like **.map()** and **.filter()**.
