# Client/Server architecture :

## How the web works :

So when we enter an adress in our browser a lot of things happen : 1. [The url gets resolved](#The-url-gets-resolved). 2. [A request is sent to the server of the website](#Request-from-server-website). 3. [The response of the server is parsed](#Response-of-the-server). 4. [The page is rendered and displayed](#Page-rendered)

### The url gets resolved

When u enter "google.com" that is called a domain, actually the server which hosts the source code of a website, is identified via IP (Internet Protocol). The browser sends a "request" to the server with the Ip address u entered.

### Request from server website

A request is a bunch of information sent to a server.

The data is sent via the "HyperText Transfer Protocol" (known as "HTTP").

The server then handles the request appropriately and returns a so called "response".Again, a "response" is smilar to a "request".

### Response of the server

In this case, the response would contain a specific piece of metadata, that tells the browser that the response if of type HTML.

### Page rendered

As mentioned, the browser goes through the HTML data returned by the server and builds a website based on that.

## Server-Side :

You need server-side programming languages that don't work in the browser but that can run on a normal computer "a server is in the end just a normal computer".

Examples: - Nodejs - PHP - Python

## Browser-Side :

In the browser, there are exactly three languages/ technologies you need to learn.
-HTML (for the structure).
-CSS (for the styling).
-Javascript (for dynamic content).

# HTTP :

-Hyper Text Transfer Protocol.
-Communication between web servers & clients.
-HTTP Requests / Responses.
-Loading pages, from submit, Ajax calls.

## HTTP Methods

- Get : Retrieves data from the server.
- Post : Submit data to the server.
- Put : Update data already on the server.
- Delete : Deletes data from the server.

## HTTP Status Codes :

    200: OK.
    201: OK created.
    301: Moved to new URL.
    304: Not modified (cached version)
    400: Bad request.
    401: Unauthorized.
    404: Not found.
    500: Internal server error.

> Postman: awesome client for building apis and testing them.
