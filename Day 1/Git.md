# Git

## Definition :

Git is a version control system for tracking changes in computer files. - Distributed version control. - Coordinates work between multiple developers. - Who made what changes and when. - Revert back at any time. - Local & remote repos.

## Concepts of Git :

    1. Keeps track of coden hitory.
    2. Takes "snapshots" of your files.
    3. You decide when to take a snapshot by making a "commit".
    4. You can visit any snapshot at any time.
    5. You can stage files before committing.

## Basic Commands :

- git init // initialize Local Git Repository.
- git add <file> // add File(s) to index.
- git status // Check status of working tree.
- git commit // Commit changes in index.
  - The goal of using a format is to :
  1. Automatic generation of the _change logs_.
  2. A better navigation through projects history (git history).
  - Message Format : <type> (<scope> #JIRATICKET) : <subject>.
  - Defining the subject (in the message format):
- git push // Push to Remote Repository.
- git pull // Pull latest from remote repository and immediately update the local repository to match that content.
- git clone // Clone Repository into a new directory.
- git branch // to create, list, rename and delete branches.
- git checkout // lets you navigate between the branches.

  - git checkout -b <new-branch> acts as a convenience method which will create the new branch and immediately switch to it.
    > You can work on multiple features in a single repository by switching between them with _git checkout_.

- git merge // take content of a source branch and integrates it with a target branch

## Git and Gitlab :

Gitlab is a Git-based fully integrated platform for software development. Gitlab has a lot of powerful features to enhance your workflow. [https://docs.gitlab.com/ee/topics/git/]
