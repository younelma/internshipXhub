import "./App.css";
import { increment, decrement } from "./redux/ducks/counter";
import { useDispatch, useSelector } from "react-redux";

function App() {
  const count = useSelector((state) => state.counter.count);
  const dispatch = useDispatch();

  const handleIncrement = () => {
    dispatch(increment());
  };
  const handleDecrement = () => {
    dispatch(decrement());
  };

  return (
    <div className="App">
      <h1>Counter Redux</h1>
      <h2 style={{ fontSize: "40px", color: "#004B57" }}>Count : {count}</h2>
      <div>
        <button onClick={handleIncrement}>Increment</button>
        <span>---</span>
        <button onClick={handleDecrement}>Decrement</button>
      </div>
    </div>
  );
}

export default App;
