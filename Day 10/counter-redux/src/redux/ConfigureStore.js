import { combineReducers, createStore } from "redux";
import { counterReducer } from "./ducks/counter";

const reducer = combineReducers({
  count: counterReducer,
});
const store = createStore(reducer);

export default store;
