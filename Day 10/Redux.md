Redux is a state management library that follows a pattern known as the Flux architecture.
in the Flux pattern, and in Redux, shared information is not stored in components but in a single object. Components are just given data to render and can request changes using events called actions.

In most applications, there are three parts:

- State: the current data used in the app.
- View: the user interface displayed to users.
- Actions: the events that a user can take to change the state.

## Actions :

An entire interaction like:
-transfer data from the input field.
-add it to an array of todos.
-render them as text on the screen.
In Redux actions are represented as plain JS objects.

## Reducers :

A reducer or reducer function, is a plain Javascript function that defines how the current state and an action are used in combination to create the new state.

Rules of reducers:

1. They should only calculate the new state value based on the state and action arguments.
2. They are not allowed to modify the existing state. Instead they must copy the existing state and make changes to the copies values.
3. They must not do any asynchronous logic or have other "side effects".

## Store :

So far we have covered state, actions, reducers, and how they participate in the one-way data flow.Where, in Javascript, does all of this take place?

Redux uses a special object called the store. The store acts as a container for state, it provides a way to dispatch actions, there will only be one store.

We can rephrase our data flow using the new term:

1. The store initializes the state with a default value.
2. The view displays that state.
3. When a user interacts with the view, like clicking a button, an action is dispatched to the store.
4. The dispatched action and current state are combined in the store's reducer to determine the next state.
5. The view is updated to display the new state.

## Redux API :

    In this lesson, you will learn how to apply the core concepts of Redux to a real Redux application.

Remember, Redux application are built upon a one-way flow of data model and are managed by the store:

1. The state is the set of data values that describes the application. It is used to render the user interface (UI)
2. Users interact with the UI which dispatch actions to the store. An action is an object that expresses a desired change to the state.
3. The store generates its next state using a reducer function which receives the most recent action and the current state as inputs.
4. Finally, the UI is re-rendered based on the new state of the store and the entire process can begin again.

in the NEXT STEP we will focus on creating a basic Redux application with the createStore() method from the Redux API and the following related store methods:

- store.getState();
- store.dispatch(action);
- store.subscribe(listener);

### Create a redux store :

Redux exports a valuable helper function for creating this store object called createStore().
The createStore() helper function has a single argument, a reducer function :

> const store = createStore(countReducer); // countReducer == a reducer of counting state.

### Dispatch actions to the store :

The store object returned by createStore() provides a number of useful methods for interacting with its state as well as the reducer function it was created with it.

The most commonly used method, store.dispatch(), can be used to dispatch an action to the store.

Action Creators:

> const toggle = () => {
> return { type: "toggle"};
> }
> store.dispatch(toggle());
> store.dispatch(toggle());
> store.dispatch(toggle());

### Respond To State Changes :

In Redux, actions dispatched to the store can be listened for and responded to using the "store.subscribe()" method.

This method accepts one argument: a function, often called a listener, that is executed in response to changes to the store's state.

> const reactToChange = () => console.log('change detected!');
> store.subscribe(reactToChange);

### Connect The Redux Store To A Ui :

1. Create a Redux store.
2. Render the initial state of the application.
3. Subscribe to updates. Inside the subscription callback.
   - Get the current store data.
   - Select the data needed by this piece of UI.
   - Update the UI with the data.
4. Respond to UI events by dispatching Redux actions.

# Complexe App Redux

In the last note, i built a simple counter app whose store state was just a single number. Though the counter app illustrates how Redux can manage the state of an application, In the next NOTE i will learn strategies for managing an application with a more complex store state.

- displays a set of recipes which are pulled from a database.
- allows the user to add/remove their favorite recipes to/from a separate list.
- allows the user to enter a search term to filter the visible recipes.

- React :

  - How to create components.
  - How to render components using ReactDOM.render().
  - How to nest components and pass data through props.

- Redux :

  - One-way data flow model: State -> View -> Actions -> State -> View ...
  - How to create a reducer function: (state, action) => nextState
  - How to write action objects and action creators.
  - How to create a store using createStore().
  - How to use the store methods getState(), dispatch(), and subscribe()

## Slices :

Redux is best suited for complex applications with many features that each have some state-related data to be managed. In these cases, objects are the go-to data types to represent the entire store's state.

In a Redux application, the top-level state properties, are known as slices. Each slice typically represents a different feature of the entire application. Notice that a slice can be any data value, like an array of objects or just a string.

> const initiaState = {
> todos: [],
> visibilityFilter: 'SHOW_ALL'
> }; // the slices are todos and visibilityFilter.

## Actions and Payloads For Complex State :

When an application state had multiple slices, individual actions typically only change one slice at a time. Therefor, it is recommended that each action's [type] follow the pattern:

    'sliceName/actionDescriptor', to clarify which slice of state should be updated.

Example:

- 'allRecipes/loadData'.
- 'favoriteRecipes/addRecipe'.

IMPORTANT !!!!! With React Redux you will learn how to solve these issues by:

- Giving the entire application access to the Redux store without using props and props drilling.
- Subscribing individual components to specific pieces of the application state for optimized rendering.
- Easily dispatching actions within components.

# REVIEW:

Let's review what I learned :

- React and Redux work well together but need more to support React’s UI optimization and Redux’s one-way data flow.
- The react-redux library provides React application components access to the Redux store
- The <Provider> component wraps around the root component to give its descendants access to the - Redux store without props drilling.
- Selectors are pure function used to access all or part of the state in the Redux store
- useSelector() retrieves the application state through selectors. It must be called from within a component.
- useSelector() subscribes components to data retrieved from the selectors. React, not Redux, re-renders those components when the selected data changes.
- useDispatch() returns a reference to Redux store dispatch() function.
