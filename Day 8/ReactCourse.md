# React EggHead Course

## User Interface with Vanilla Javascript

Create a user interface with vanilla javascript and DOM, in the JavascriptUI.js file.

## User Interface with React's JSX syntax.

Create a user interface with React jsx syntax in the js file ReactUI.js

## Babel :

is a javascript compiler that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of Javascript in current and older browsers or environments.

### styling React Component :

This applies to more than styling. It is a great example of creating versatile components through abstraction.

> custom components are how you reduce reptition in your code.
