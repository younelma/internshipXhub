const rootElement = document.getElementById("root");

const element1 = React.createElement("div", {
  children: "Hello World",
  className: "container",
});
/** const children = 'Hello World'
 * const className = 'container'
with a javascript compiler: const element = <div className={className}>{children}</div>
*/
ReactDOM.render(element1, rootElement);

// Render two elements with React Fragments

const helloElement = React.createElement("span", null, "Hello");
const worldElement = React.createElement("span", null, "World");

const element2 = React.createElement(
  React.Fragment,
  null,
  helloElement,
  worldElement
);
/** const element = (
        <React.Fragment> // or open brackets like this <></>
            <span>Hello</span><span>World</span>
        </React.Fragment>
)*/
ReactDOM.render(element2, document.getElementById("root"));

// Create a simple Reusable React Component.
const Message = (props) => <div className="message">{props.message}</div>;
const element3 = (
  <div className="container">
    <Message msg="Hello world" />
    <Message>
      Hello World
      <Message>Goodbye World</Message>
      <span>I'm Here</span>
    </Message>
    {/* <Message>Hello World</Message>
    {Message({ msg: "Hello World" })}
    {Message({ msg: "Goodbye World" })} */}
  </div>
);

ReactDOM.render(element3, document.getElementById("root"));

// Validate Custom React Component Props with PropTypes

function SayHello({ firstName, lastName }) {
  return (
    <div>
      Hello {firstName} {lastName}!
    </div>
  );
}

SayHello.propTypes = {
  firstName(props, propName, componentName) {
    if (typeof props[propName] !== "string") {
      return new Error(
        `Hey, the component ${componentName} needs the prop ${propName} to be a string, but you passed a ${typeof props[
          propName
        ]}`
      );
    }
  },
};

// Understand and use interpolation in JSX
function CharacterCount({ text }) {
  return (
    <div>
      {`The text "${text}" has `}{" "}
      <strong>{text.length ? text.length : "No"}</strong> charachters
    </div>
  );
}

// Style React Components with className and inline Styles.
function Box({ className = "", style, size, ...rest }) {
  const sizeClassName = size ? `box--${size}` : "";
  return (
    <div
      className={`box ${className}`}
      style={{ fontStyle: "italic", ...style }}
      {...rest}
    />
  );
}
