Some of the core features of jest including: 1. Installing and configuring Jest. 2. Creating tests using the test() function 3. Testing asynchronous code 4. Creating and using mocks within your test.

## TDD :

Benefits of TDD:

- You get testable code by default.
- You get high code coverage as a default.
- Defects are caught earlier.
- TDD leads to a cleaner design with very modular, low-coupled code.
- You get executable documentation in the form of unit tests.
