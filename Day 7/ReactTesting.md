This course "React Testing with Jest" is on codeAcademy.

## Jest

Jest focuses on simplicity, Jest provides the two key ingredients needed for testing :
1- An assertion library - an API of functions for validating a program's functionality.
2- A test runner - a tool that executes tests and provides outputted test summaries.

## Matcher Functions

we all know the **expect()** assertion function along with the **.toEqual()** matcher method. Let's learn about a few more common matcher methods.

- **.toBeDefined()** is used to verify that a variable is not _undefined_. This is often the first thing checked.
- **.toEqual()** is used to perform deep equality checks between objects.
- **.toBe()** is Similar to **.toEqual()**
- **.toBeTruthy()** is used to verify whether a value is truthy or not.
- **.not()** is used before another matcher to verify that the opposite result is true.
- **.toContain()** is used when we want to verify that an item is an array.

## Review :

- Jest is an easy-to-use framework for testing in a Javascript environment beacuase it combines a test-runner with assertion methods like the **expect()** API.
- The basic syntax involved with creating a simple unit test, such as the **test()** function.
- The realm of testing asynchronous code with Jest by using the done parameter to wait for asynchronous callbacks and the **async/await** keywords to wait for Promises to resolve.
- How to mock functions using jest.fn() and make use of mock functions using **jest.fn()** and make use of mocked modules with **jest.mock()** by mocking the _Axios module_.
