As React developers, we love breaking down complex problems into simple pieces.

Classes, however, are not simple. They:

- Are difficult to reuse between components
- Are tricky and time-consuming to test
- Have confused many developers and caused lots of bugs

React offers a number of built-in Hooks. A few of these include useState(), useEffect(), useContext(), useReducer(), and useRef()...
