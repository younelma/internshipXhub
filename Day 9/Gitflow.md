Gitflow is an alternative Git branching model that involves the use of feature branches and multiple primary branches.

# How it works :

## Develop and main branches

Instead of single _main_ branch, this workflow usesn two branches to record the history of the project. The _main_ branch stores the official release history, and the _develop_ branch serves as an integration branch for features. It's also convenient to tag all commits in the main branch with a version number.

A simple way to complement the default _main_ with a _develop_ branch locally and push it to the server:

> git branch develop
> git push -u origin develop

**Features** branches are generally created off to the latest _develop_ branch.

## Creating a feature branch

without the git-flow extensions :

> git checkout develop
> git checkout -b feature_branch

## Finishing a feature branch

When we are done with the development work on the feature, the next step is to merge the _feature-branch_ into develop.

Without the git-flow extensions :

> git checkout develop
> git merge feature-branch

## Release branches

Once _develop_ has acquired enough features for a release (or a predetermined release date is approaching), you fork a _release_ branch off of _develop_. Creating this branch starts the next release cycle, so no new features can be added after this point----only bug fixes, documentation generation, and other release-oriented tasks should go in this branch. Once it's ready to ship, the _release_ branch gets merged into _main_ and tagged with a version number. In addition, it should be merged back into develop, which may have progressed since the release was initiated.

> git checkout develop
> git checkout -b release/0.1.0

## Hotflix branches :

Maintenance or "hotfix" branches are used to quickly patch production releases. **Hotfix** branches are a lot like _release_ branches and _feature_ branches except they're based on _main_ instead of _develop_.
This is the only branch that should fork directly off of _main_.
As soon as the fix is complete, it should be merged into both _main_ and _develop_ (or the current _release_ branch), and _main_ should be tagged with an updated version number.

> git checkout main
> git checkout -b hotfix_branch

Similar to finishing a _release_ branch, a _hotfix_ branch gets merged into both _main_ and _develop_.

> git checkout main
> git merge hotfix_branch
> git checkout develop
> git merge hotfix_branch
> git branch -D hotfix_branch

# Summary

Gitflow is one of many styles of Git workflows you and your team can utilize :D.

Some key takeaways to know about Gitflow are:

1. The workflow is great for a release-based software workflow.
2. Gitflow offers a dedicated channel for hotfixes to production.

The overall flow of Gitflow is:

1. A _develop_ branch is created from main.
2. A _release_ branch = is created from _develop_.
3. _Feature_ branches are created from _develop_.
4. When a _feature_ is complete it is merged into the _develop_ branch.
5. When the _release_ branch is done it is merged into _develop_ and _main_.
6. If an issue in _main_ is detected a _hotfix_ branch is created from _main_.
7. Once the _hotfix_ is complete it is merged to both _develop_ and _main_.
